from django.db import models
from django.utils import timezone
from django.shortcuts import reverse

class Category(models.Model):
    title = models.CharField('Заголовок', max_length=255)
    slug = models.SlugField('Ссылка', unique=True)
    image = models.SlugField('Картинка', null=True, blank=True)

    
    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Катергории'
    def get_absolute_url(self):
        return reverse('category_detail_url', kwargs={'slug':self.slug})
    
    def str(self):
        return self.title

class Post(models.Model):
    title = models.CharField('Заголовок', max_length=255)
    image = models.ImageField('Картинка', blank=True, null=True)
    slug = models.SlugField('Ссылка', unique=True)
    views = models.IntegerField('Просмотри', default=0)
    category = models.ForeignKey(Category, on_delete = models.CASCADE, null=True, verbose_name="Категории")
    summary = models.TextField('Краткое описание')
    text = models.TextField('Полное описание')
    views = models.IntegerField('Просмотри', default=0)
    date = models.DateField('Дата', default=timezone.now)

    def get_absolute_url(self):
        return reverse('post_detail_url', kwargs={'slug':self.slug})


    class Meta:
        verbose_name = 'Новость'
        verbose_name_plural = 'Новости'

    def __str__(self):
        return self.title


class FeedBack(models.Model):
    name = models.CharField('Ф.И.О', max_length=255)
    phone = models.CharField('Телефон', max_length=255)
    email = models.EmailField('Email')
    text = models.TextField('Сообщение')
    date = models.DateTimeField('Дата', default=timezone.now)

    class Meta:
        verbose_name='Сообщение'
        verbose_name_plural='Сообщения'

    def __str__(self):
        return self.name

class Comment(models.Model):
    post = models.ForeignKey(Post, on_delete = models.CASCADE)
    author_name = models.CharField('Имя автора', max_length = 50)
    comment_text = models.TextField('Текст комментария', max_length=1000)

def __str__(self):
    return self.author_name

class Meta:
    verbose_name = 'Комментарий'
    verbose_name_plural = 'Комментарии'
