from django.shortcuts import render, redirect
from django.db.models import Q
from .models import Post, Category, FeedBack
from .forms import RegisterForm

def index(request):
    popular = Post.objects.filter(views__gt = 10)[:3]
    return render(request, 'news/index.html', {'popular':popular})

def news(request):
    posts = Post.objects.order_by('-date')
    return render(request, 'news/news.html', {'posts':posts})

def popular(request):
    if request.method == "POST":
        result = request.POST.get('popular')
        posts = Post.objects.order_by(result)
        return render(request, 'news/news.html', {'posts':posts})
    return redirect('news')


def search(request):
    query = request.GET.get('search')
    search_result = Post.objects.filter(
        Q(title__icontains = query)
    )

def post_detail(request, slug):
    post = Post.objects.get(slug__iexact = slug)
    post.views += 1
    post.save()
    return render(request, 'news/post_detail.html', {'post':post})

def categories(request):
    categories = Category.objects.order_by('title')
    return render(request, 'news/categories.html', {'categories': categories})

def category_detail(request, slug):
    category = Category.objects.get(slug__iexact = slug)
    return render(request, 'news/category_detail.html', {'category':category})

def register(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('index')
    else:
        form = RegisterForm()
    return render(request, 'news/register.html', {'forms':form})

def contact(request):
    if request.method == "POST":
        FeedBack.objects.create(
            name = request.POST.get('name'),
            phone = request.POST.get('phone'),
            email = request.POST.get('email'),
            text = request.POST.get('text'),
        )

        return redirect('contact')
    return render(request, 'news/contact.html')

def leave_comment(request, slug):
    try:
        post = Post.objects.get(slug__iexact = slug)
    except:
        raise Http404("Статья не найдена")
    post.comment_set.create(autor_name = request.POST.get('name'), comment_text = request.POST.get('text'))
    return redirect( reverse('post_detail', args = (slug,)))


